import pytest
import pytest-cov
import pytest-env
import pytest-mock
import pytest-runner

class InsufficientAmount(Exception):
    pass


class Wallet(object):

    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def spend_cash(self, amount):
        if self.balance < amount:
            raise InsufficientAmount('Not enough available to spend {}'.format(amount))
        self.balance -= amount

    def add_cash(self, amount):
        self.balance += amount
        
def capital_case(x):
    return x.capitalize()
    

def test_addition():
  assert 1 + 1 == 2


def test_subtraction():
  diff = 1 - 1
  assert diff == 1
  
def test_capital_case():
    assert capital_case('semaphore') == 'Semaphore'
    
def test_default_initial_amount():
    wallet = Wallet()
    assert wallet.balance == 0

def test_setting_initial_amount():
    wallet = Wallet(100)
    assert wallet.balance == 100

def test_wallet_add_cash():
    wallet = Wallet(10)
    wallet.add_cash(90)
    assert wallet.balance == 100

def test_wallet_spend_cash():
    wallet = Wallet(20)
    wallet.spend_cash(10)
    assert wallet.balance == 10
    
    