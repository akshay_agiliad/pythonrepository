import unittest 

def sum(arg):
    total = 0
    for val in arg:
        total += val
    return total


class TestSum(unittest.TestCase):
    def test_list_int(self):
        """
        Test that it can sum a list of integers
        """
        data = [1, 2, 3]
        result = sum(data)
        self.assertEqual(result, 6)

    def test_list_int_1(self):
        """
        Test that it can sum a list of integers
        """
        data = [1, 2, 5, 9]
        result = sum(data)
        self.assertEqual(result, 17)

    def test_list_int_2(self):
        """
        Test that it can sum a list of integers
        """
        data = [1, 5, 9]
        result = sum(data)
        self.assertEqual(result, 15)
        
  
    def test_list_int_3(self):
        """
        Test that it can sum a list of integers
        """
        data = [1, 5, 9,100]
        result = sum(data)
        self.assertEqual(result, 115)


if __name__ == '__main__':
    mymodule.set_verbose( True )
    unittest.main()
